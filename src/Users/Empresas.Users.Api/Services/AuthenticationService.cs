﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using NetDevPack.Security.JwtSigningCredentials.Interfaces;
using Empresas.Users.Api.Data;
using Empresas.Users.Api.Models;
using Empresas.WebApi.Core.User;
using Empresas.Users.Api.Extensions;
using Empresas.WebApi.Core.Authentication;
using System.Text;

namespace Empresas.Users.Api.Services
{
    public class AuthenticationService
    {
        public readonly SignInManager<IdentityUser> SignInManager;
        public readonly UserManager<IdentityUser> UserManager;
        private readonly AppTokenSettings _appTokenSettingsSettings;
        private readonly UsersContext _context;

        public AuthenticationService(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IOptions<AppTokenSettings> appTokenSettingsSettings,
            UsersContext context)
        {
            SignInManager = signInManager;
            UserManager = userManager;
            _appTokenSettingsSettings = appTokenSettingsSettings.Value;
            _context = context;
        }

        public async Task<UserLoginResponse> GenerateJwt(string name)
        {
            var user = await UserManager.FindByNameAsync(name);
            var claims = await UserManager.GetClaimsAsync(user);
            var identityClaims = await GetUserClaims(claims, user);
            var encodedToken = EncodeToken(identityClaims);
            var refreshToken = await GenerateRefreshToken(name);

            return GetTokenResponse(encodedToken, user, claims, refreshToken);
        }

        public async Task<RefreshToken> GetRefreshToken(Guid refreshToken)
        {
            var token = await _context.RefreshTokens.AsNoTracking()
                .FirstOrDefaultAsync(u => u.Token == refreshToken);

            return token != null && token.ExpirationDate > DateTime.Now
                ? token
                : null;
        }

        public async Task<bool> IsAdmin(string name)
        {
            var user = await UserManager.FindByNameAsync(name);
            var claims = await UserManager.GetClaimsAsync(user);
            
            return claims.Any(c => c.Type.Equals("Admin") && c.Value.Equals("S"));
        }

        public async Task<bool> IsDisable(string name)
        {
            var user = await UserManager.FindByNameAsync(name);
            
            return user.LockoutEnabled;
        }

        public async Task<bool> ValidatePassword(string name, string password)
        {
            var validate = await SignInManager.PasswordSignInAsync(name, password, false, true);

            return validate.Succeeded;
        }

        private async Task<ClaimsIdentity> GetUserClaims(ICollection<Claim> claims, IdentityUser user)
        {
            var userRoles = await UserManager.GetRolesAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.NameId, user.UserName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(DateTime.UtcNow).ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64));
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim("role", userRole));
            }

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            return identityClaims;
        }

        private string EncodeToken(ClaimsIdentity identityClaims)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("PASSWORD-EMPRESAS");
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = "Empresas",
                Audience = "http://localhost",
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            return tokenHandler.WriteToken(token);
        }

        private UserLoginResponse GetTokenResponse(string encodedToken, IdentityUser user, 
            IEnumerable<Claim> claims, RefreshToken refreshToken)
        {
            return new UserLoginResponse
            {
                AccessToken = encodedToken,
                RefreshToken = refreshToken.Token,
                ExpiresIn = TimeSpan.FromHours(2).TotalSeconds,
                UserToken = new UserToken
                {
                    Id = user.Id,
                    Name = user.UserName,
                    Email = user.Email,
                    Claims = claims.Select(c => new UserClaim { Type = c.Type, Value = c.Value })
                }
            };
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                .TotalSeconds);

        private async Task<RefreshToken> GenerateRefreshToken(string name)
        {
            var refreshToken = new RefreshToken
            {
                Username = name,
                ExpirationDate = DateTime.UtcNow.AddHours(_appTokenSettingsSettings.RefreshTokenExpiration)
            };

            _context.RefreshTokens.RemoveRange(_context.RefreshTokens.Where(u => u.Username == name));
            await _context.RefreshTokens.AddAsync(refreshToken);

            await _context.SaveChangesAsync();

            return refreshToken;
        }
    }
}