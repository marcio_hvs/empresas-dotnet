﻿namespace Empresas.Users.Api.Extensions
{
    public class AppTokenSettings
    {
        public int RefreshTokenExpiration { get; set; }
    }
}
