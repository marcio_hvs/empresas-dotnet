﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using Empresas.Users.Api.Models;
using Empresas.Users.Api.Services;
using Empresas.WebApi.Core.Controllers;
using System.Collections.Generic;

namespace Empresas.Users.Api.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : MainController
    {
        private readonly AuthenticationService _authenticationService;
        private readonly UserManager<IdentityUser> _userManager;

        public UsersController(AuthenticationService authenticationService, UserManager<IdentityUser> userManager)
        {
            _authenticationService = authenticationService;
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        [SwaggerOperation(Summary = "Efetuar login",
                          Description = "Realiza a autenticação do usuário no sistema.")]
        public async Task<ActionResult> Login(UserLogin userLogin)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
            
            if (!await ValidateUser(userLogin.Name, userLogin.Password)) return CustomResponse();

            var result = await _authenticationService.SignInManager.PasswordSignInAsync(userLogin.Name, userLogin.Password, false, true);

            if (result.Succeeded)
            {
                return CustomResponse(await _authenticationService.GenerateJwt(userLogin.Name));
            }

            if (result.IsLockedOut)
            {
                AddErrorProcessing("Usuário temporariamente bloqueado por tentativas inválidas");
                return CustomResponse();
            }

            AddErrorProcessing("Usuário ou Senha incorretos");
            return CustomResponse();
        }

        [AllowAnonymous]
        [HttpPost("new-account")]
        [SwaggerOperation(Summary = "Nova conta",
                          Description = "Cria uma nova conta de usuário no sistema.")]
        public async Task<ActionResult> NewAccount(UserRegistration userRegistration)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = new IdentityUser
            {
                UserName = userRegistration.Name,
                Email = userRegistration.Email,
                EmailConfirmed = true
            };

            var result = await _authenticationService.UserManager.CreateAsync(user, userRegistration.Password);

            if (result.Succeeded) return CustomResponse(await _authenticationService.GenerateJwt(userRegistration.Name));

            foreach (var error in result.Errors)
            {
                AddErrorProcessing(error.Description);
            }

            return CustomResponse();
        }

        [AllowAnonymous]
        [HttpPost("refresh-token")]
        [SwaggerOperation(Summary = "Atualiza Token",
                          Description = "Atualizar o token do usuário.")]
        public async Task<ActionResult> RefreshToken([FromBody] string refreshToken)
        {
            if (string.IsNullOrEmpty(refreshToken))
            {
                AddErrorProcessing("Refresh Token inválido");
                return CustomResponse();
            }

            var token = await _authenticationService.GetRefreshToken(Guid.Parse(refreshToken));

            if (token is null)
            {
                AddErrorProcessing("Refresh Token expirado");
                return CustomResponse();
            }

            return CustomResponse(await _authenticationService.GenerateJwt(token.Username));
        }

        [AllowAnonymous]
        [HttpPut("change-password")]
        [SwaggerOperation(Summary = "Alterar senha",
                          Description = "Altera a senha de acesso ao sistema do usuário.")]
        public async Task<ActionResult> ChangePassword(UserNewPassword userNewPassword)
        {
            if (!await ValidateUser(userNewPassword.Name, userNewPassword.OldPassword)) return CustomResponse();

            var user = await _userManager.FindByNameAsync(userNewPassword.Name);
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, userNewPassword.NewPassword);

            return CustomResponse(result);
        }

        [AllowAnonymous]
        [HttpPut("edit-user")]
        [SwaggerOperation(Summary = "Editar Usuário",
                          Description = "Altera os dados do usuário.")]
        public async Task<ActionResult> EditUser(UserRegistration userRegistration)
        {
            if (!await ValidateUser(userRegistration.Name, userRegistration.Password)) return CustomResponse();

            var user = await _userManager.FindByNameAsync(userRegistration.Name);

            user.UserName = userRegistration.Name;
            user.Email = userRegistration.Email;

            var result = await _userManager.UpdateAsync(user);

            return CustomResponse(result);
        }

        //[AllowAnonymous]
        [HttpPut("claim")]
        [SwaggerOperation(Summary = "Adicionar/Altera Claim",
                          Description = "Adiciona Claims ao usuário ou altera o valor de uma Claim.")]
        public async Task<ActionResult> Claim(string userName, string claimType, string claimValue)
        {
            var user = await _userManager.FindByNameAsync(userName);
            
            if (user == null)
            {
                AddErrorProcessing("Usuário não existe");
                return CustomResponse();
            }

            var claim = (await _userManager.GetClaimsAsync(user))
                .FirstOrDefault(c => c.Type.Equals(claimType));

            if (claim != null) await _userManager.RemoveClaimAsync(user, claim);

            var result = await _userManager.AddClaimAsync(user, new Claim(claimType, claimValue));

            return CustomResponse(result);
        }

        //[AllowAnonymous]
        [HttpPut("lockoutEnabled")]
        [SwaggerOperation(Summary = "Bloquear Usuário",
                          Description = "Bloqueia um determinado usuário.")]
        public async Task<ActionResult> LockoutEnabled(UserLogin userLogin)
        {
            if (!await ValidateUser(userLogin.Name, userLogin.Password)) return CustomResponse();

            var user = await _userManager.FindByNameAsync(userLogin.Name);
            var result = await _userManager.SetLockoutEnabledAsync(user, true);

            return CustomResponse(result);
        }

        [AllowAnonymous]
        [HttpGet("paged-query/{userName}&{pageNumber}")]
        [SwaggerOperation(Summary = "Consulta Paginada",
                          Description = "Lista todos os usuários.")]
        public async Task<ActionResult> PagedQuery(string userName, int pageNumber)
        {
            if (!await _authenticationService.IsAdmin(userName))
            {
                AddErrorProcessing("Você não possui permissão de administrador");
                return CustomResponse();
            }

            var usersAll = _userManager.Users.Where(u => !u.LockoutEnabled);
            var admins = new List<string>();

            foreach (var user in usersAll)
            {
                var claims = await _userManager.GetClaimsAsync(user);
                if (claims.Any(c => c.Type.Equals("Admin") && c.Value.Equals("S"))) admins.Add(user.UserName);
            }

            var users = usersAll
                .Where(u => !admins.Contains(u.UserName))
                .Select(u => new UserDefault() { Id = u.Id, Name = u.UserName, Email = u.Email })
                .OrderBy(u => u.Name);

            var result = await PaginatedList<UserDefault>.CreateAsync(users, pageNumber, 3);

            return CustomResponse(result);
        }

        private async Task<bool> ValidateUser(string name, string password)
        {
            if(await _authenticationService.IsDisable(name))
            {
                AddErrorProcessing("Usuário não existe");
                return false;
            }

            var result = await _authenticationService.ValidatePassword(name, password);
            if(!result) AddErrorProcessing("Usuário ou Senha incorretos");

            return result;
        }
    }
}