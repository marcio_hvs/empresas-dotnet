﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Empresas.WebApi.Core.User
{
    public class AspNetUser : IAspNetUser
    {
        private readonly IHttpContextAccessor _accessor;

        public AspNetUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Name => _accessor.HttpContext.User.Identity.Name;

        public Guid GetUserId()
        {
            return Authenticated() ? Guid.Parse(_accessor.HttpContext.User.GetUserId()) : Guid.Empty;
        }

        public string GetUserEmail()
        {
            return Authenticated() ? _accessor.HttpContext.User.GetUserEmail() : "";
        }

        public string GetUserLogin()
        {
            return Authenticated() ? _accessor.HttpContext.User.GetUserNameId() : "";
        }

        public string GetUserName()
        {
            return Authenticated() ? _accessor.HttpContext.User.GetUserName() : "";
        }

        public string GetUserToken()
        {
            return Authenticated() ? _accessor.HttpContext.User.GetUserToken() : "";
        }

        public bool Authenticated()
        {
            return _accessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public bool Administrator()
        {
            var claims = _accessor.HttpContext.User.Claims;

            return claims.Any(c => c.Type.Equals("Administrator") && c.Value.Equals("S"));
        }

        public bool HasRole(string role)
        {
            return _accessor.HttpContext.User.IsInRole(role);
        }

        public IEnumerable<Claim> GetClaims()
        {
            return _accessor.HttpContext.User.Claims;
        }

        public HttpContext GetHttpContext()
        {
            return _accessor.HttpContext;
        }
    }
}
