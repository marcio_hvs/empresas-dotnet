﻿namespace Empresas.WebApi.Core.Authentication
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int Expires { get; set; }
        public string Issuer { get; set; }
        public string ValidOn { get; set; }
    }
}
