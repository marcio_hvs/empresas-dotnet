﻿using MediatR;
using System.Threading.Tasks;
using FluentValidation.Results;
using Empresas.Core.Messages;


namespace Empresas.Core.Mediator
{
    public class MediatorHandler : IMediatorHandler
    {
        private readonly IMediator _mediator;

        public MediatorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ValidationResult> SendCommand<T>(T commandItem) where T : Command
        {
            return await _mediator.Send(commandItem);
        }

        public async Task PublishEvent<T>(T eventItem) where T : Event
        {
            await _mediator.Publish(eventItem);
        }
    }
}