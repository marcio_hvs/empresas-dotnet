﻿using System.Threading.Tasks;
using FluentValidation.Results;
using Empresas.Core.Messages;

namespace Empresas.Core.Mediator
{
    public interface IMediatorHandler
    {
        Task PublishEvent<T>(T eventItem) where T : Event;
        Task<ValidationResult> SendCommand<T>(T commandItem) where T : Command;
    }
}