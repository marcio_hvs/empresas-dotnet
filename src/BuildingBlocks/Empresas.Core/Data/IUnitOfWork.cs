﻿using System.Threading.Tasks;

namespace Empresas.Core.Data
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}