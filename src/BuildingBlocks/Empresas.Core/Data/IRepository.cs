﻿using System;
using Empresas.Core.DomainObjects;

namespace Empresas.Core.Data
{
    public interface IRepository<T> : IDisposable where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}