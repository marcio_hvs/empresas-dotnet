﻿using System.Text.Json.Serialization;

namespace Empresas.Core.DomainObjects.Enums
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Gender
    {
        Action,
        Adventure,
        ArtCinema,
        Chanchada,
        Comedy,
        ActionComedy,
        HorrorComedy,
        DramaticComedy,
        RomanticComedy,
        Dance,
        Documentary,
        Documentation,
        Drama,
        Espionage,
        Western,
        Fantasy,
        ScientificFantasy,
        ScienceFiction,
        MoviesWithTricks,
        WarMovies,
        Musical,
        Thriller,
        Romance,
        Sitcom,
        Suspense,
        Horror
    }
}
