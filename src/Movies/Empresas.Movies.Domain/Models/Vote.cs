﻿using System;
using Empresas.Core.DomainObjects;

namespace Empresas.Movies.Domain.Models
{
    public class Vote : Entity
    {
        public string UserName { get; private set; }
        public int Value { get; private set; }
        public Guid MovieId { get; private set; }

        // EF Relation
        public Movie Movie { get; private set; }

        //EF
        protected Vote() { }

        public Vote(Guid id, string userName, int value, Guid movieId)
        {
            Id = id;
            UserName = userName;
            Value = value;
            MovieId = movieId;
        }
    }
}
