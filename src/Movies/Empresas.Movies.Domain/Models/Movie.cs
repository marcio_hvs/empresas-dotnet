﻿using System;
using System.Collections.Generic;
using Empresas.Core.DomainObjects;
using Empresas.Core.DomainObjects.Enums;

namespace Empresas.Movies.Domain.Models
{
    public class Movie : Entity, IAggregateRoot
    {
        public string Title { get; private set; }
        public Gender Gender { get; private set; }
        public string Director { get; private set; }
        public int CountVotes { get; private set; }
        public decimal AverageVotes { get; private set; }

        //EF Relation
        public ICollection<Vote> Votes { get; private set; }

        //EF
        protected Movie() { }

        public Movie(Guid id, string title, Gender gender, string director, 
            int countVotes, decimal averageVotes)
        {
            Id = id;
            Title = title;
            Gender = gender;
            Director = director;
            CountVotes = countVotes;
            AverageVotes = averageVotes;
        }

        public void UpdateValues(int count, decimal average)
        {
            CountVotes = count;
            AverageVotes = average;
        }
    }
}
