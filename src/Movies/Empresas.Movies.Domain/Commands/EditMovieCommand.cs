﻿using System;
using Empresas.Core.DomainObjects.Enums;
using Empresas.Movies.Domain.Validations;

namespace Empresas.Movies.Domain.Commands
{
    public class EditMovieCommand : MovieCommand
    {
        public EditMovieCommand(Guid id, string title, string director, Gender gender)
        {
            Id = id;
            Title = title;
            Director = director;
            Gender = gender;
        }

        public override bool Valid()
        {
            ValidationResult = new EditMovieCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
