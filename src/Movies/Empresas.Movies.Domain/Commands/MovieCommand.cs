﻿using System;
using Empresas.Core.Messages;
using Empresas.Core.DomainObjects.Enums;

namespace Empresas.Movies.Domain.Commands
{
    public abstract class MovieCommand : Command
    {
        public Guid Id { get; protected set; }
        public string Title { get; protected set; }
        public string Director { get; protected set; }
        public Gender Gender { get; protected set; }
    }
}
