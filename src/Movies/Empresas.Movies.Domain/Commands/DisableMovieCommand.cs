﻿using System;
using Empresas.Movies.Domain.Validations;

namespace Empresas.Movies.Domain.Commands
{
    public class DisableMovieCommand : MovieCommand
    {
        public DisableMovieCommand(Guid id)
        {
            Id = id;
        }

        public override bool Valid()
        {
            ValidationResult = new DisableMovieCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
