﻿using System;
using Empresas.Core.Messages;

namespace Empresas.Movies.Domain.Commands
{
    public abstract class VoteCommand : Command
    {
        public Guid Id { get; protected set; }
        public string UserName { get; protected set; }
        public int Value { get; protected set; }
        public Guid MovieId { get; protected set; }
    }
}
