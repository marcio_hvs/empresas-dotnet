﻿using System;
using Empresas.Movies.Domain.Validations;

namespace Empresas.Movies.Domain.Commands
{
    public class AddVoteCommand : VoteCommand
    {
        public AddVoteCommand(Guid id, string userName, int value, Guid movieId)
        {
            Id = id;
            UserName = userName;
            Value = value;
            MovieId = movieId;
        }

        public override bool Valid()
        {
            ValidationResult = new AddVoteCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
