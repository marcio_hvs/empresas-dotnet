﻿using System;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Empresas.Core.Messages;
using FluentValidation.Results;
using Empresas.Movies.Domain.Models;
using Empresas.Movies.Domain.Commands;
using Empresas.Movies.Domain.Interfaces;

namespace Empresas.Movies.Domain.CommandHandlers
{
    public class VoteCommandHandler : CommandHandler,
        IRequestHandler<AddVoteCommand, ValidationResult>
    {
        private readonly IMovieRepository _movieRepository;

        public VoteCommandHandler(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<ValidationResult> Handle(AddVoteCommand message, CancellationToken cancellationToken)
        {
            if (!message.Valid()) return message.ValidationResult;

            var voteId = message.Id == Guid.Empty ? Guid.NewGuid() : message.Id;
            var vote = new Vote(voteId, message.UserName, message.Value, message.MovieId);
            _movieRepository.AddVote(vote);

            var movie = await _movieRepository.GetMovieById(message.MovieId);
            var votes = await _movieRepository.GetVotesByMovie(message.MovieId);
            movie.UpdateValues(votes.Count(), Convert.ToDecimal(votes.Average(v => v.Value)));
            _movieRepository.EditMovie(movie);

            return await PersistData(_movieRepository.UnitOfWork);
        }
    }
}
