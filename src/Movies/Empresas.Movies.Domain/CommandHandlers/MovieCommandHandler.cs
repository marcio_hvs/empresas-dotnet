﻿using System;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Empresas.Core.Messages;
using FluentValidation.Results;
using Empresas.Movies.Domain.Models;
using Empresas.Movies.Domain.Commands;
using Empresas.Movies.Domain.Interfaces;
using System.Linq;

namespace Empresas.Movies.Domain.CommandHandlers
{
    public class MovieCommandHandler : CommandHandler,
        IRequestHandler<AddMovieCommand, ValidationResult>,
        IRequestHandler<EditMovieCommand, ValidationResult>,
        IRequestHandler<DisableMovieCommand, ValidationResult>
    {
        private readonly IMovieRepository _movieRepository;

        public MovieCommandHandler(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<ValidationResult> Handle(AddMovieCommand message, CancellationToken cancellationToken)
        {
            if (!message.Valid()) return message.ValidationResult;

            var movieId = message.Id == Guid.Empty ? Guid.NewGuid() : message.Id;

            var movie = new Movie(movieId, message.Title, message.Gender, message.Director, 0, 0M);

            _movieRepository.AddMovie(movie);

            return await PersistData(_movieRepository.UnitOfWork);
        }

        public async Task<ValidationResult> Handle(EditMovieCommand message, CancellationToken cancellationToken)
        {
            if (!message.Valid()) return message.ValidationResult;

            var votes = await _movieRepository.GetVotesByMovie(message.Id);
            var value = votes == null ? 0 : votes.Count();
            var average = votes == null ? 0M : Convert.ToDecimal(votes.Average(v => v.Value));

            var movie = new Movie(message.Id, message.Title, message.Gender, message.Director, value, average);

            _movieRepository.EditMovie(movie);

            return await PersistData(_movieRepository.UnitOfWork);
        }

        public async Task<ValidationResult> Handle(DisableMovieCommand message, CancellationToken cancellationToken)
        {
            if (!message.Valid()) return message.ValidationResult;

            _movieRepository.DisableMovie(message.Id);

            return await PersistData(_movieRepository.UnitOfWork);
        }
    }
}
