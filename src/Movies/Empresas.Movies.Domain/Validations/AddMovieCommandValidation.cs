﻿using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public class AddMovieCommandValidation : MovieCommandValidation<AddMovieCommand>
    {
        public AddMovieCommandValidation()
        {
            ValidateId();
            ValidateTitle();
            ValidateDirector();
            ValidateGender();
        }
    }
}
