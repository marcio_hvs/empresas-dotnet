﻿using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public class EditMovieCommandValidation : MovieCommandValidation<EditMovieCommand>
    {
        public EditMovieCommandValidation()
        {
            ValidateTitle();
            ValidateDirector();
            ValidateGender();
        }
    }
}
