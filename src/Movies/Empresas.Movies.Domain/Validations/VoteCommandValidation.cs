﻿using System;
using FluentValidation;
using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public abstract class VoteCommandValidation<T> : AbstractValidator<T> where T : VoteCommand
    {
        protected void ValidateId()
        {
            RuleFor(v => v.Id)
                .NotEqual(Guid.Empty).WithMessage("O campo Id possui valor inválido");
        }

        protected void ValidateUserName()
        {
            RuleFor(v => v.UserName)
                .NotEmpty().WithMessage("O campo UserName não foi informado")
                .Length(1, 200).WithMessage("O campo UserName deve possuir entre 1 a 200 caracteres");
        }

        protected void ValidateValue()
        {
            RuleFor(v => v.Value)
                .InclusiveBetween(0, 4).WithMessage("O campo Value deve ser entre 0 a 4");
        }

        protected void ValidateMovieId()
        {
            RuleFor(v => v.MovieId)
                .NotEqual(Guid.Empty).WithMessage("O campo MovieId possui valor inválido");
        }
    }
}
