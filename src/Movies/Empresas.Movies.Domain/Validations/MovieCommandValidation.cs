﻿using System;
using FluentValidation;
using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public abstract class MovieCommandValidation<T> : AbstractValidator<T> where T : MovieCommand
    {
        protected void ValidateId()
        {
            RuleFor(m => m.Id)
                .NotEqual(Guid.Empty).WithMessage("O campo Id possui valor inválido");
        }

        protected void ValidateTitle()
        {
            RuleFor(m => m.Title)
                .NotEmpty().WithMessage("O campo Title não foi informado")
                .Length(1, 200).WithMessage("O campo Title deve possuir entre 1 a 200 caracteres");
        }

        protected void ValidateDirector()
        {
            RuleFor(m => m.Director)
                .NotEmpty().WithMessage("O campo Director não foi informado")
                .Length(1, 200).WithMessage("O campo Director deve possuir entre 1 a 200 caracteres");
        }

        protected void ValidateGender()
        {
            RuleFor(m => m.Gender)
                .IsInEnum().WithMessage("O campo Gender possui valor inválido");
        }
    }
}
