﻿using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public class AddVoteCommandValidation : VoteCommandValidation<AddVoteCommand>
    {
        public AddVoteCommandValidation()
        {
            ValidateId();
            ValidateUserName();
            ValidateValue();
            ValidateMovieId();
        }
    }
}
