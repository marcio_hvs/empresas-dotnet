﻿using Empresas.Movies.Domain.Commands;

namespace Empresas.Movies.Domain.Validations
{
    public class DisableMovieCommandValidation : MovieCommandValidation<DisableMovieCommand>
    {
        public DisableMovieCommandValidation()
        {
            ValidateId();
        }
    }
}
