﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Empresas.Core.Data;
using Empresas.Movies.Domain.Models;
using Empresas.Core.DomainObjects.Enums;

namespace Empresas.Movies.Domain.Interfaces
{
    public interface IMovieRepository : IRepository<Movie>
    {
        #region Movie
        Task<IEnumerable<Movie>> GetMovies();
        Task<Movie> GetMovieById(Guid id);
        Task<IEnumerable<Movie>> GetMoviesByGender(Gender gender);
        Task<IEnumerable<Movie>> GetMoviesByDirector(string director);
        void AddMovie(Movie movie);
        void EditMovie(Movie movie);
        void DisableMovie(Guid id);
        #endregion

        #region Vote
        Task<IEnumerable<Vote>> GetVotes();
        Task<IEnumerable<Vote>> GetVotesByMovie(Guid movieId);
        void AddVote(Vote vote);
        #endregion
    }
}
