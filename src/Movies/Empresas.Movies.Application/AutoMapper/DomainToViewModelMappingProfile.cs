﻿using AutoMapper;
using Empresas.Movies.Domain.Models;
using Empresas.Movies.Application.ViewModels;

namespace Empresas.Movies.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Movie, MovieViewModel>();
            CreateMap<Vote, VoteViewModel>();
        }
    }
}
