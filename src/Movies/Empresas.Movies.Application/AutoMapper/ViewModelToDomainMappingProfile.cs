﻿using AutoMapper;
using Empresas.Movies.Domain.Models;
using Empresas.Movies.Application.ViewModels;

namespace Empresas.Movies.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<MovieViewModel, Movie>()
                .ConstructUsing(m => new Movie(m.Id, m.Title, m.Gender, m.Director, m.CountVotes, m.AverageVotes));

            CreateMap<VoteViewModel, Vote>()
                .ConstructUsing(v => new Vote(v.Id, v.UserName, v.Value, v.MovieId));
        }
    }
}