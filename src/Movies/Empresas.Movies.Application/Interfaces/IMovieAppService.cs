﻿using System;
using System.Threading.Tasks;
using FluentValidation.Results;
using System.Collections.Generic;
using Empresas.Core.DomainObjects.Enums;
using Empresas.Movies.Application.ViewModels;

namespace Empresas.Movies.Application.Interfaces
{
    public interface IMovieAppService : IDisposable
    {
        #region Movie
        Task<IEnumerable<MovieViewModel>> GetMovies();
        Task<MovieViewModel> GetMovieById(Guid id);
        Task<IEnumerable<MovieViewModel>> GetMoviesByGender(Gender gender);
        Task<IEnumerable<MovieViewModel>> GetMoviesByDirector(string director);
        Task<ValidationResult> AddMovie(MovieViewModel movie);
        Task<ValidationResult> EditMovie(MovieViewModel movie);
        Task<ValidationResult> DisableMovie(Guid id);
        #endregion

        #region Vote
        Task<IEnumerable<VoteViewModel>> GetVotes();
        Task<IEnumerable<VoteViewModel>> GetVotesByMovie(Guid movieId);
        Task<ValidationResult> AddVote(VoteViewModel vote);
        #endregion
    }
}
