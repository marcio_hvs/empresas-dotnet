﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using FluentValidation.Results;
using System.Collections.Generic;
using Empresas.Core.Mediator;
using Empresas.Movies.Domain.Commands;
using Empresas.Core.DomainObjects.Enums;
using Empresas.Movies.Domain.Interfaces;
using Empresas.Movies.Application.Interfaces;
using Empresas.Movies.Application.ViewModels;

namespace Empresas.Movies.Application.Services
{
    public class MovieAppService : IMovieAppService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMediatorHandler _mediatorHandler;
        private readonly IMapper _mapper;

        public MovieAppService(IMovieRepository movieRepository,
                               IMediatorHandler mediatorHandler,
                               IMapper mapper)
        {
            _movieRepository = movieRepository;
            _mediatorHandler = mediatorHandler;
            _mapper = mapper;
        }

        #region Movie
        public async Task<IEnumerable<MovieViewModel>> GetMovies()
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepository.GetMovies());
        }

        public async Task<MovieViewModel> GetMovieById(Guid id)
        {
            return _mapper.Map<MovieViewModel>(await _movieRepository.GetMovieById(id));
        }

        public async Task<IEnumerable<MovieViewModel>> GetMoviesByGender(Gender gender)
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepository.GetMoviesByGender(gender));
        }

        public async Task<IEnumerable<MovieViewModel>> GetMoviesByDirector(string director)
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepository.GetMoviesByDirector(director));
        }

        public async Task<ValidationResult> AddMovie(MovieViewModel movie)
        {
            var comando = new AddMovieCommand(movie.Id, movie.Title, movie.Director, movie.Gender);

            return await _mediatorHandler.SendCommand(comando);
        }

        public async Task<ValidationResult> EditMovie(MovieViewModel movie)
        {
            var comando = new EditMovieCommand(movie.Id, movie.Title, movie.Director, movie.Gender);

            return await _mediatorHandler.SendCommand(comando);
        }

        public async Task<ValidationResult> DisableMovie(Guid id)
        {
            var comando = new DisableMovieCommand(id);

            return await _mediatorHandler.SendCommand(comando);
        }
        #endregion

        #region Vote
        public async Task<IEnumerable<VoteViewModel>> GetVotes()
        {
            return _mapper.Map<IEnumerable<VoteViewModel>>(await _movieRepository.GetVotes());
        }

        public async Task<IEnumerable<VoteViewModel>> GetVotesByMovie(Guid movieId)
        {
            return _mapper.Map<IEnumerable<VoteViewModel>>(await _movieRepository.GetVotesByMovie(movieId));
        }

        public async Task<ValidationResult> AddVote(VoteViewModel vote)
        {
            var comando = new AddVoteCommand(vote.Id, vote.UserName, vote.Value, vote.MovieId);

            return await _mediatorHandler.SendCommand(comando);
        }
        #endregion

        public void Dispose()
        {
            _movieRepository?.Dispose();
        }
    }
}
