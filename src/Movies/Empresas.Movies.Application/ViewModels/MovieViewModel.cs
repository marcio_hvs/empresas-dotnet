﻿using System;
using Empresas.Core.DomainObjects.Enums;

namespace Empresas.Movies.Application.ViewModels
{
    public class MovieViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Gender Gender { get; set; }
        public string Director { get; set; }
        public int CountVotes { get; private set; }
        public decimal AverageVotes { get; private set; }
    }
}
