﻿using System;

namespace Empresas.Movies.Application.ViewModels
{
    public class VoteViewModel
    {
        public Guid Id { get; set; }
        public string UserName { get; private set; }
        public int Value { get; set; }
        public Guid MovieId { get; set; }

        public void AddUserName(string userName)
        {
            UserName = userName;
        }
    }
}
