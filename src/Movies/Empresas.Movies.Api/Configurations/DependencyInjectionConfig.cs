﻿using MediatR;
using FluentValidation.Results;
using Microsoft.Extensions.DependencyInjection;
using Empresas.Core.Mediator;
using Empresas.Movies.Data.Context;
using Empresas.Movies.Domain.Commands;
using Empresas.Movies.Domain.Interfaces;
using Empresas.Movies.Data.Repositories;
using Empresas.Movies.Application.Services;
using Empresas.Movies.Application.Interfaces;
using Empresas.Movies.Domain.CommandHandlers;
using Empresas.WebApi.Core.User;
using Microsoft.AspNetCore.Http;

namespace Empresas.Movies.Api.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            //HttpContext
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAspNetUser, AspNetUser>();

            // Mediator
            services.AddScoped<IMediatorHandler, MediatorHandler>();

            // Domain
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IMovieAppService, MovieAppService>();
            services.AddScoped<MoviesContext>();

            //Movie
            services.AddScoped<IRequestHandler<AddMovieCommand, ValidationResult>, MovieCommandHandler>();
            services.AddScoped<IRequestHandler<EditMovieCommand, ValidationResult>, MovieCommandHandler>();
            services.AddScoped<IRequestHandler<DisableMovieCommand, ValidationResult>, MovieCommandHandler>();

            //Vote
            services.AddScoped<IRequestHandler<AddVoteCommand, ValidationResult>, VoteCommandHandler>();
            //services.AddScoped<INotificationHandler<AddedVoteEvent>, VoteEventHandler>();

            services.AddScoped<IMediatorHandler, MediatorHandler>();
        }
    }
}