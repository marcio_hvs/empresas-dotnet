﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Empresas.WebApi.Core.User;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using Empresas.WebApi.Core.Controllers;
using Empresas.Core.DomainObjects.Enums;
using Empresas.Movies.Application.Interfaces;
using Empresas.Movies.Application.ViewModels;
using Empresas.Core.DomainObjects;

namespace Empresas.Movies.Api.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class VotesController : MainController
    {
        private readonly IMovieAppService _movieAppService;
        private readonly IAspNetUser _aspNetUser;

        public VotesController(IMovieAppService movieAppService,
            IAspNetUser aspNetUser)
        {
            _movieAppService = movieAppService;
            _aspNetUser = aspNetUser;
        }

        [AllowAnonymous]
        [HttpGet()]
        [SwaggerOperation(Summary = "Lista Votos",
                          Description = "Lista todos os votos.")]
        public async Task<ActionResult> GetMovies()
        {
            var votes = await _movieAppService.GetVotes();

            if (votes == null) return NotFound();

            return CustomResponse(votes);
        }

        [AllowAnonymous]
        [HttpPost()]
        [SwaggerOperation(Summary = "Adicionar Voto",
                          Description = "Adiciona voto ao Filme.")]
        public async Task<ActionResult> AddVote(VoteViewModel voteViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse();

            var token = DecodeJWT();

            voteViewModel.AddUserName(token.Claims.Where(c => c.Type.Equals("nameid")).FirstOrDefault().Value);

            var result = await _movieAppService.AddVote(voteViewModel);

            return CustomResponse(result);
        }

        private JwtSecurityToken DecodeJWT()
        {
            var authorizationHeader = _aspNetUser.GetHttpContext().Request.Headers["Authorization"];
            var tokenString = authorizationHeader.ToString();
            var jwtEncodedString = tokenString.Substring(7);
            var token = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);

            return token;
        }
    }
}
