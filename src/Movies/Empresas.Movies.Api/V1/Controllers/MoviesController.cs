﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Empresas.WebApi.Core.User;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using Empresas.WebApi.Core.Controllers;
using Empresas.Core.DomainObjects.Enums;
using Empresas.Movies.Application.Interfaces;
using Empresas.Movies.Application.ViewModels;

namespace Empresas.Movies.Api.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class MoviesController : MainController
    {
        private readonly IMovieAppService _movieAppService;
        private readonly IAspNetUser _aspNetUser;

        public MoviesController(IMovieAppService movieAppService,
            IAspNetUser aspNetUser)
        {
            _movieAppService = movieAppService;
            _aspNetUser = aspNetUser;
        }

        [HttpGet()]
        [SwaggerOperation(Summary = "Lista Filmes",
                          Description = "Lista todos os filmes.")]
        public async Task<ActionResult> GetMovies()
        {
            var movies = await _movieAppService.GetMovies();

            if (movies == null) return NotFound();

            return CustomResponse(movies);
        }

        [HttpGet("by-gender/{gender}")]
        [SwaggerOperation(Summary = "Lista Filmes por Gênero",
                          Description = "Lista todos os filmes de um determinado gênero.")]
        public async Task<ActionResult> GetMoviesByGender(Gender gender)
        {
            var movies = await _movieAppService.GetMoviesByGender(gender);

            if (movies == null) return NotFound();

            return CustomResponse(movies.OrderBy(m => m.Title).OrderBy(m => m.CountVotes));
        }

        [HttpGet("by-director/{director}")]
        [SwaggerOperation(Summary = "Lista Filmes por Diretor",
                          Description = "Lista todos os filmes de um determinado diretor.")]
        public async Task<ActionResult> GetMoviesByDirector(string director)
        {
            var movies = await _movieAppService.GetMoviesByDirector(director);

            if (movies == null) return NotFound();

            return CustomResponse(movies.OrderBy(m => m.Title).OrderBy(m => m.CountVotes));
        }

        [HttpPost()]
        [SwaggerOperation(Summary = "Adicionar Filme",
                          Description = "Adiciona cadastro do Filme.")]
        public async Task<ActionResult> AddMovie(MovieViewModel movieViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse();

            var token = DecodeJWT();

            if(!token.Claims.Any(c => c.Type.Equals("Admin") && c.Value.Equals("S")))
            {
                AddErrorProcessing("Você não tem permissão de administrador");
                return CustomResponse();
            }

            var result = await _movieAppService.AddMovie(movieViewModel);

            return CustomResponse(result);
        }

        private JwtSecurityToken DecodeJWT()
        {
            var authorizationHeader = _aspNetUser.GetHttpContext().Request.Headers["Authorization"];
            var tokenString = authorizationHeader.ToString();
            var jwtEncodedString = tokenString.Substring(7);
            var token = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);

            return token;
        }
    }
}
