﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Empresas.Core.Data;
using Empresas.Movies.Data.Context;
using Empresas.Movies.Domain.Models;
using Empresas.Movies.Domain.Interfaces;
using Empresas.Core.DomainObjects.Enums;

namespace Empresas.Movies.Data.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MoviesContext _context;

        public MovieRepository(MoviesContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        #region Movie
        public async Task<IEnumerable<Movie>> GetMovies()
        {
            return await _context.Movies.AsNoTracking()
                .Include(m => m.Votes)
                .Where(m => m.Active)
                .ToListAsync();
        }

        public async Task<Movie> GetMovieById(Guid id)
        {
            return await _context.Movies.AsNoTracking()
                .Include(m => m.Votes)
                .Where(m => m.Active && m.Id.Equals(id))
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesByGender(Gender gender)
        {
            return await _context.Movies.AsNoTracking()
                .Include(m => m.Votes)
                .Where(m => m.Active && m.Gender.Equals(gender))
                .ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesByDirector(string director)
        {
            return await _context.Movies.AsNoTracking()
                .Include(m => m.Votes)
                .Where(m => m.Active && m.Director.Equals(director))
                .ToListAsync();
        }

        public void AddMovie(Movie movie)
        {
            _context.Movies.Add(movie);
        }

        public void EditMovie(Movie movie)
        {
            _context.Movies.Update(movie);
        }

        public void DisableMovie(Guid id)
        {
            var movie = _context.Movies.Find(id);
            movie.Disable();

            _context.Movies.Update(movie);
        }
        #endregion

        #region Vote
        public async Task<IEnumerable<Vote>> GetVotes()
        {
            return await _context.Votes.AsNoTracking()
                .Where(v => v.Active)
                .ToListAsync();
        }

        public async Task<IEnumerable<Vote>> GetVotesByMovie(Guid movieId)
        {
            return await _context.Votes.AsNoTracking()
                .Where(v => v.Active && v.MovieId.Equals(movieId))
                .ToListAsync();
        }

        public void AddVote(Vote vote)
        {
            _context.Votes.Add(vote);
        }
        #endregion

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
