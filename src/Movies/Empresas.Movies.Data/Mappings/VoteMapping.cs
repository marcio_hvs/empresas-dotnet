﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Empresas.Movies.Domain.Models;

namespace Empresas.Movies.Data.Mappings
{
    public class VoteMapping : IEntityTypeConfiguration<Vote>
    {
        public void Configure(EntityTypeBuilder<Vote> builder)
        {
            builder.HasKey(v => v.Id);

            builder.Property(v => v.UserName)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(v => v.Value)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(v => v.Active)
                .IsRequired()
                .HasColumnType("bit")
                .HasDefaultValue(true);

            builder.Property(v => v.CadastreDate)
                .IsRequired()
                .HasColumnType("datetime");

            // 1 : N => Movie : Vote
            builder.HasOne(v => v.Movie)
                .WithMany(v => v.Votes);

            builder.ToTable("Votes");
        }
    }
}
