﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Empresas.Movies.Domain.Models;

namespace Empresas.Movies.Data.Mappings
{
    public class MovieMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Title)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(m => m.Director)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(m => m.CountVotes)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(m => m.AverageVotes)
                .IsRequired()
                .HasColumnType("numeric(3,2)");

            builder.Property(m => m.Active)
                .IsRequired()
                .HasColumnType("bit")
                .HasDefaultValue(true);

            builder.Property(m => m.CadastreDate)
                .IsRequired()
                .HasColumnType("datetime");

            // 1 : N => Movie : Vote
            builder.HasMany(m => m.Votes)
                .WithOne(m => m.Movie)
                .HasForeignKey(m => m.MovieId);

            builder.ToTable("Movies");
        }
    }
}
